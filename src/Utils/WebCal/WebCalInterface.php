<?php

namespace App\Utils\WebCal;

interface WebCalInterface
{
    public function export(): string;
}
