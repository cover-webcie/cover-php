<?php

namespace App\Legacy\Database;

use App\Legacy\Database\DataIter;

class GenericDataIter extends DataIter
{
    static public function fields()
    {
        return [];
    }
}
