import './location_picker';
import './sticker_map';

// Include Mapbox CSS. Be sure to include the "map" link tags in the template!
import 'mapbox-gl/dist/mapbox-gl.css';
